---
title: 'NudgeEntrance: Intervention to change hand washing behaviour'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - paper
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''
download:
	url: p2_paper.pdf
	alt: Published paper
---

Dhamelia M, Dalvi G. (2017). NudgeEntrance: Intervention to change hand washing behaviour, IEEE SeGAH 2018

===

## Abstract
Hands are primary vectors of many communicable diseases because they come in contact with a variety of surfaces and fluids. Acute diarrhoeal diseases and respiratory diseases can be prevented by washing hands at appropriate times. This habit is essential for households of urban low-income groups since their surroundings are a hotbed for pathogens. The effect of hand washing is also acknowledged by experts and the world health organisation (WHO). We propose a design based intervention employing behaviour change principles suitable for such households. The goal of the intervention was to nudge people to wash hands at appropriate times via self reflection. Our intervention induces self reflection through reminders and channel factors. We measured hand washing frequency to evaluate the change in behavior, and further investigated this habit through interviews. Results of our study indicate that channel factors bring significant change in hand washing behaviour than reminders.