---
title: Pre-test/post-test of my thoughts: intervention: IDC

---

This post documents experiences, effects, and influences of design education, of IDC on me. A bittersweet symphony as it was, some myths about the design education are debunked, some hypothesis are formed, some ideas are generated. It quotes some people at IDC, and how they influenced me in my thought processes. This post includes a pre-test/post-test analysis of my thoughts. 

===

#### A prequel
I entered IDC as a believer of randomness, unstructured in thoughts, one thing I was certain that a good education is one that allows you to self-criticise and makes you capable to look objectively at the world and yourself. 