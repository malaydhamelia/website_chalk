---
title: 'On Art/Design'
headline: ''
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
published: false
publish_date: '08-08-2017 20:17'
taxonomy:
    category:
        - blog
    tag:
        - reflections
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

Consider a scene in an art gallery. There is a well curated exhibit of sculptures and paintings. These all are created by the same person on the theme of her travel experiences. Subjectivity at its best. But, how is she going to present it ? What should be the story she wants to tell to people ?

Design is a tool that helps you achieve something desired, systematically. Art, not to be poetic, can/cannot be self-expression, from a skeptic and purist’s viewport. Art, is mostly design.  Is it possible that an art piece becomes an art piece because you(the artist) achieved what you(the artist) sought, but you(the artist) dont(does not) know how ? If so, may I boldly say that design for the visceral is easily confused for an art form. Probably, I am a rationalist, since I am trying so hard to find purpose in everything. But hey! what’s the harm, if I am using reason and knowledge as a tool to understand things ?

So, to convince you, I want the readers to comment with an piece they consider as art with source and briefly reason about its classification as an art piece.

To exemplify, I would ask, is graphical vandalism, an art ? Is it a design ?
![](Girl-and-a-Solder-by-Banksy.jpg)