---
title: 'Designing artefacts for fiction'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'

taxonomy:
    category:
        - blog
    tag:
        - howTo
        - fiction
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

publish_date: 4/9/2017 1:00 pm
published: true
---

This post will describe the process behind creation of the artefact for design fiction. This post also elaborates on my interpretations and opinions about the course _Trends in Interactive Technologies_ by Prof. Venkat Rajamanickam. 

===

This post in under draft. Please visit the [artefact](https://github.com/femoris/Liron-Liron/wiki/Main-Page) meanwhile.