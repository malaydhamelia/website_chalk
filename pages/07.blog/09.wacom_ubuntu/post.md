---
title: 'Wacom with ubuntu [No pressure sensitivity]'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - howTo
        - towardsOpenSource
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

publish_date: 10/10/2017 8:30 pm
published: false
---

A how to guide for using wacom with *nixy systems. 

===

Reading around the history of raspberry pi and initial media coverage directed me towards the Slashdot effect of the 90’s. Distributor websites of Raspi succumbed to the [slashdot effect](https://en.wikipedia.org/wiki/Jargon_File) and were temporarily unavailable. Unaware of slashdot effect, I turned to its Wikipedia, where I came across this fabulous document called ‘The Jargon File’ (another source: [Jargon file website](http://catb.org/jargon/html/). I downloaded “The new hacker’s dictionary” a lexicon for the hacker slang—developed and maintained by the hackers since the 1970s. [Manybooks.net](http://manybooks.net/) gave me the book in many file formats (mobi file format for my kindle and txt for typesetting).
For typesetting, the first thought was of using Latex and build a typeset using command line. But, I also wanted to learn Scribus, an open-source DTP tool. Also, I wanted to explore my recent purchase of Wacom Intuos Comic—a touch-enabled graphics tablet.
Intuos does not work as a mouse out of the box on Linux machines. So, I had to install the drivers for the tablet. I had done this before but never using a Linux machine on a command line. I came across the input-wacom driver which is a set of modules loadable to the kernel. So, while plugging in the tablet, I ran following commands

````
sudo apt-get install linux-headers-$(uname -r) build-essential
tar xjvf input-wacom-<version number>.tar.bz2
cd input-wacom-<version number>
````

Building

````
./configure 
make 
sudo make install 
````
Loading modules
````
sudo modprobe -r wacom
sudo modprobe -r wacom_w8001
sudo modprobe wacom
sudo modprobe wacom_w8001
````
For verifying
````
cat /sys/module/wacom*/version 
````
The output should be something like: v-2.0.0-0.37.1 and not just v-2.0.0

I tried using Grammarly and Hemingway for this post and the commands I came across were referenced from the [Linux-Wacom tablet Wiki](http://linuxwacom.sourceforge.net/wiki/index.php/Input-wacom).

To be really useful to the open-source creatives, there needs to be a procedure for using wacom with its full functionality. Above mentioned steps only allow pointing capabilities of wacom, however, keep looking to this space...

A tiny step towards open-source workflow in the visual/creative space...
