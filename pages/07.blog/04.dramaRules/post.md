---
title: 'Rules de drama'
headline: ''
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
published: false
publish_date: '23-08-2017 17:50'
taxonomy:
    category:
        - blog
    tag:
        - realisations
        - fundae
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

![](capture-2.gif)

===
1. Everyone is a participant of a drama. 
1. Everyone plays. They play well.

What is playing well ? Who defines the rules ?

1. Everyone plays well, and the drama goes smooth if everyone played their part. 
Who sets the roles ?
1. Everything plays fine until you play by their rules. 
What if you broke their rules ?
