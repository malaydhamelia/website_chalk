---
publish_date: 9/10/2017 1:00 pm
title: 'SEZs, Freights and Railways'
media_order: '99.jpg,Accueil568bdf48566c5.jpg'
taxonomy:
    category:
        - blog
    tag:
        - howTo
        - visualisation
        - data
        - playground
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
headline: ''

publish_date: 9/10/2017 1:00 pm
---

This post documents the process during DE705 — Interactive Data Visualisation (offered by Prof [Venkatesh Rajamanickam](https://info-design-lab.github.io/)) at IDC. The goal of assignment was to create a visualisation or a data story or a tool using the dataset provided. As fascinated I was with dataset ‘mashups’, I planned to mash the given dataset with few more datasets. 

===

I have provided the URLs to the datasets which I used and their sample tables are also inserted for the sake of easy readability of the article. 

Datasets:

####Intermittent Stations list of Indian Railways (ISLds, hereon)

The data set has a format as shown in the table below

![](1.png)

The source of the data set is [here](https://data.gov.in/catalog/indian-railways-train-time-table-0). Although they are the same, [this](https://docs.google.com/spreadsheets/d/1gqIJQ8QLYLVYVjzep-v69x--Y3XQqEPBfnAokobKw9g/edit?usp=sharing) is the reduced dataset used for this story (Sheet 2 contains the original dataset from the source, whereas Sheet 1 contains the set which is used)

####List of Operational SEZs (referred as SEZds, from hereon)

This is a plotted dataset, meaning, the dataset was created using plotting. This data set was created by plotting the addresses to Google Maps using Awesome Tables. The map is then exported to Keyhole Markup Language (KML) and then converted to CSV using an online parser <https://mygeodata.cloud/>, because CSV is human readable format. This was necessary as data set formation is yet to be done. The final set can be found over here(Final Dataset 1). It looks like [this](https://docs.google.com/spreadsheets/d/1lVUChZjCiLtNRbWkQTW-RbxvfA62289W1O6oExqSeeA/edit?usp=sharing)

![](2.png)

####Container Rail Terminal (CRT) List (CRTds, from hereon)

A CRT is functionally a railway station that may be used for passenger exchange, but are primarily used for freight exhange. This dataset contains the list of CRT located in different parts of India. This is a derived dataset. First, a list of dataset was created using a python script with beautiful soup. The source is [Freight Operation Information System (FOIS)](https://www.fois.indianrail.gov.in/fois/Q_CRTLocnInput.jsp). The dataset obtained is hosted here and looks like [this](https://docs.google.com/spreadsheets/d/1YMtjoNVRJD7qqPsK4jGyDoNZUlefv7yBCuT6GnnZwtY/edit#gid=0),

![](3.png)

So, in summary, we have 3 datasets, one which is geocoded (SEZ ds) and CRTds has list of stations but they are not geocoded. So, we need a dataset that has station names or codes with their latitudes and longitudes. Luckily there is a dataset on Github by Sajjad Anwar who goes by the name of geohacker. This was provided to the class by Prof. Find it [here](https://github.com/datameet/railways).

So, one more dataset…

####Stations.json (Stationsds, for short)

This dataset was converted to CSV file and can be found [here (Sheet 1)](https://docs.google.com/spreadsheets/d/1cMuyo5t4DZLyLnehPjoCt3xUnHSmtShQIbDxseuS3cw/edit?usp=sharing). It looks like this, though…

![](4.png)

Now, the stationsds and CRTds are to be mapped since CRTs are essentially stations. I used VLOOKUP to search for the CRT from a cell, find the name in a table that consists of Station Code and its LatLongs to get the co-ordinates of CRTs. This whole sheet with computation can be accessed [here (Sheet2)](<https://docs.google.com/spreadsheets/d/1cMuyo5t4DZLyLnehPjoCt3xUnHSmtShQIbDxseuS3cw/edit?usp=sharing>). As you might have noticed, some CRTs donot have geocodes in the stationsds itself. 

To cure this malady, awesome tables comes to the rescue. I got the names of the stations using filters and used awesome table add-on to find the rest of the geocodes. The final data set of CRTs with their geocodes is [here (Final Dataset 2)](<https://docs.google.com/spreadsheets/d/1RHa-gKamY5o4rnqOvvM70w_9EIxcXmgAxsO_og-5Uw4/edit?usp=sharing>) and they look like this 

![](5.png)

For the impatient, you can now plot using the poison of your choice (leaflet, mapbox, osm, so forth). Make sure to use two different legends.

####Merging the ISLds

While exploring this dataset, I found out that there are certain stations, where no passenger train passes. I achieved that by counting the number of cells containing a particular CRT. This computation is done in in this sheet (Sheet 1). It looks something like this

Now, I used a filter to find out stations where no passenger train passes or the last column is zero. This dataset is available [here (Final Dataset 3)](<https://docs.google.com/spreadsheets/d/1lVUChZjCiLtNRbWkQTW-RbxvfA62289W1O6oExqSeeA/edit#gid=0>)

It suggests that, such stations are only used for the freight exchange and nothing else. Now the questions that can be asked to the visualisation is why is that such stations only carry goods and nothing else? With the datasets in hand, possibility of station dedicated to SEZ can be eliminated or considered depending on the proximity of SEZ from station.

Now, plot all the datasets into different layers of your tool, I have used mapbox here. It is shown below. Here, white dots are for SEZs, orange are for CRTs while red are for CRTs with no passengers.

###[Mapbox visualisation](https://api.mapbox.com/styles/v1/mewron/cj7ayg5209lao2rpp0qhak6uq.html?title=true&access_token=pk.eyJ1IjoibWV3cm9uIiwiYSI6ImNqNnlwbmx2NzA2N2MycWxyc3F5YWRsM3oifQ.bk_ZWzBocHQ_zUjj_tJl5Q#3.9/21.98/80.86)

####Observations

Three cases with reference to the SEZs in focus.

+White dots and red dots are closer to each other as shown below near Akola and Nashik.
    ![](6.png)
+Many SEZs, very less dedicated CRTs for SEZs
    ![](7.png)
+Many CRTs, no SEZs (3 Dots in line). This will allow into the use of CRTs apart from freight exchange. Freight can be for supply of goods or armament supply or used by military.
    ![](8.png)
+Uttarakhand and Himachal have almost no CRTs, but, for military freights, there are several CRTs in the region.

####Some edits needed here and there:

+Plotting using leafletjs for the love of opensource. It will also provide me with customisability to have tooltips
+Layering. Switching on/off layers for better user control
+Limitation of this visualisation is that the dataset used for plotting stations (stationsds) is 2 years old and doesnot contain all stations. So, scouting for a new dataset should be the first step from here.